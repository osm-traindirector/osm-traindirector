/*	Track.h - Created by Giampiero Caprino

This file is part of Train Director 3

Train Director is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; using exclusively version 2.
It is expressly forbidden the use of higher versions of the GNU
General Public License.

Train Director is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Train Director; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
*/

#ifndef _TRACK_H
#define _TRACK_H

#include "tdscript.h"

class Track;

class TrackInterpreterData : public InterpreterData {
public:
	TrackInterpreterData()
	{
	    _onInit = 0;
	    _onSetBusy = 0;
	    _onSetFree = 0;
	    _onEnter = 0;
	    _onExit = 0;
	    _onClicked = 0;
	    _onCrossed = 0;
	}

	~TrackInterpreterData()
	{
	    if(_onInit)
		delete _onInit;
	    if(_onSetBusy)
		delete _onSetBusy;
	    if(_onSetFree)
		delete _onSetFree;
	    if(_onEnter)
		delete _onEnter;
	    if(_onExit)
		delete _onExit;
	    if(_onClicked)
		delete _onClicked;
	    if(_onCrossed)
		delete _onCrossed;
	};

	Statement *_onInit;	// list of actions (statements)
	Statement *_onSetBusy;
	Statement *_onSetFree;
	Statement *_onEnter;
	Statement *_onExit;
	Statement *_onClicked;
	Statement *_onCrossed;

	bool	Evaluate(ExprNode *expr, ExprValue& result);
	bool	GetNextPath(Track *, Vector **path);
};



class Track : public TrackBase {
public:

	Track() { };
	virtual ~Track() { };

	void	OnInit();	// initial setting (when load or restart)

	void	OnSetBusy();	// track has become busy

	void	OnSetFree();	// track has become free

	void	OnEnter(Train *trn);	// train entered track

	void	OnExit(Train *trn);	// train left track

	void	OnClicked();		// user clicked on track

	void	OnCrossed(Train *trn);	// train activated a trigger

	void	ParseProgram();		// fill TrackInterpreterData

	void	RunScript(const wxChar *script, Train *trn = 0);

	void	FreeProgram();		// release TrackInterpreterData

	bool	GetPropertyValue(const wxChar *prop, ExprValue& result);
	bool	SetPropertyValue(const wxChar *prop, ExprValue& val);

	void	SetColor(grcolor color);
	bool	IsBusy() const;
};

#endif // _TRACK_H
