/*	Itinerary.h - Created by Giampiero Caprino

This file is part of Train Director 3

Train Director is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; using exclusively version 2.
It is expressly forbidden the use of higher versions of the GNU
General Public License.

Train Director is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Train Director; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
*/

#ifndef _ITINERARY_H
#define _ITINERARY_H

typedef struct {
	int	x, y;		/* coordinate of the switch */
	int	switched;	/* whether to automatically throw the switch */
	int	oldsw;		/* old status */
} switin;

class Itinerary {
public:
	Itinerary *next;
	int	visited;	/* flag to avoid endless loop */
	wxChar	*name;		/* name of itinerary */
	wxChar	*signame;	/* name of start signal */
	wxChar	*endsig;	/* name of end signal */
	wxChar	*nextitin;	/* next itinerary automatically activated */
	int	nsects, maxsects;/* sections are signal-to-signal */
	switin	*sw;

	void	OnInit();	// script support
	void	OnClick();	// script support

	bool	GetPropertyValue(const wxChar *prop, ExprValue& result);

	void	*_interpreterData;
};

class	Track;

void	itinerary_selected(Itinerary *it);
void	itinerary_selected(Track *t);

void	delete_itinerary(Itinerary *it);
void	delete_itinerary(const wxChar *name);
void	free_itinerary(Itinerary *it);
void	add_itinerary(Itinerary *it, int x, int y, int switched);
void	try_itinerary(int sx, int sy, int ex, int ey);
void	sort_itineraries();

extern	Itinerary *itineraries;

#endif // _ITINERARY_H
