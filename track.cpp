/*	track.cpp - Created by Giampiero Caprino

This file is part of Train Director 3

Train Director is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; using exclusively version 2.
It is expressly forbidden the use of higher versions of the GNU
General Public License.

Train Director is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Train Director; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
*/

#include <stdio.h>
#include <string.h>

#if !defined(__unix__) && !defined(__WXMAC__)
#include <malloc.h>
#endif

#include "Traindir3.h"
#include "Itinerary.h"
#include "itracks.h"
#include "itools.h"
#include "isignals.h"
#include "iswitch.h"
#include "iactions.h"
#include "imovers.h"

extern	void	draw_itin_text(int x, int y, const wxChar *txt, int size);

extern	void	delete_script_data(TrackBase *t);

extern	const	Char *GetColorName(int color);

#define OLD		    /* old way of drawing tracks */

int	terse_status;

extern	int	current_tool;
extern	int	layout_modified;    /* user edited the layout */

grcolor	fieldcolors[MAXFIELDCOL];

int	auto_link = 1;
int	show_links = 1;
Coord	link_start;
Coord	move_start, move_end;
int	current_macro = -1;
wxChar	*current_macro_name;
Track	**macros;
int	nmacros, maxmacros;

VLines	n_s_layout[] = {
	{ HGRID / 2 + 1, 0, HGRID / 2 + 1, VGRID - 1 },
	{ HGRID / 2 - 0, 0, HGRID / 2 - 0, VGRID - 1 },
	{ -1 }
};
SegDir	n_s_segs[] = { SEG_N, SEG_S, SEG_END };
SegDir	sw_n_segs[] = { SEG_SW, SEG_N, SEG_END };
SegDir	nw_s_segs[] = { SEG_NW, SEG_S, SEG_END };
SegDir	w_e_segs[] = { SEG_W, SEG_E, SEG_END };
SegDir	nw_e_segs[] = { SEG_NW, SEG_E, SEG_END };
SegDir	sw_e_segs[] = { SEG_SW, SEG_E, SEG_END };
SegDir	w_ne_segs[] = { SEG_W, SEG_NE, SEG_END };
SegDir	w_se_segs[] = { SEG_W, SEG_SE, SEG_END };
SegDir	nw_se_segs[] = { SEG_NW, SEG_SE, SEG_END };
SegDir	sw_ne_segs[] = { SEG_SW, SEG_NE, SEG_END };
SegDir	ne_s_segs[] = { SEG_NE, SEG_S, SEG_END };
SegDir	se_n_segs[] = { SEG_SE, SEG_N, SEG_END };
SegDir	itin_segs[] = { SEG_NW, SEG_SW, SEG_NE, SEG_SE, SEG_W, SEG_E, SEG_END };

VLines	sw_n_layout[] = {
	{ HGRID / 2 + 1, 0, HGRID / 2 + 1, VGRID / 2 },
	{ HGRID / 2 - 0, 0, HGRID / 2 - 0, VGRID / 2 },
	{ HGRID / 2 + 1, VGRID / 2, 1, VGRID - 1 },
	{ HGRID / 2 - 0, VGRID / 2, 0, VGRID - 1 },
	{ HGRID / 2 - 0, VGRID / 2 - 1, 0, VGRID - 2 },
	{ -1 }
};

VLines	nw_s_layout[] = {
	{ 1, 0, HGRID / 2 + 1, VGRID / 2 },
	{ 0, 0, HGRID / 2 - 0, VGRID / 2 },
	{ 0, 1, HGRID / 2 - 1, VGRID / 2 },
	{ HGRID / 2 + 1, VGRID / 2 - 0, HGRID / 2 + 1, VGRID - 1 },
	{ HGRID / 2 - 0, VGRID / 2 - 0, HGRID / 2 - 0, VGRID - 1 },
	{ -1 }
};

VLines	se_n_layout[] = {
	{ HGRID / 2 + 1, 0, HGRID / 2 + 1, VGRID / 2 },
	{ HGRID / 2 - 0, 0, HGRID / 2 - 0, VGRID / 2 },
	{ HGRID / 2 + 1, VGRID / 2, HGRID - 1, VGRID - 2 },
	{ HGRID / 2 - 0, VGRID / 2, HGRID - 1, VGRID - 1 },
	{ HGRID / 2 - 0, VGRID / 2 + 1, HGRID - 2, VGRID - 1 },
	{ -1 }
};

VLines	ne_s_layout[] = {
	{ HGRID / 2, VGRID / 2 - 1, HGRID - 2, 0 },
	{ HGRID / 2, VGRID / 2, HGRID - 1, 0 },
	{ HGRID / 2 + 1, VGRID / 2, HGRID - 1, 1 },
	{ HGRID / 2 + 1, VGRID / 2 - 0, HGRID / 2 + 1, VGRID - 1 },
	{ HGRID / 2 - 0, VGRID / 2 - 0, HGRID / 2 - 0, VGRID - 1 },
	{ -1 }
};

VLines	w_e_layout[] = {
	/*{ 0, VGRID / 2 - 1, HGRID - 1, VGRID / 2 - 1 },*/
	{ 0, VGRID / 2 - 0, HGRID - 1, VGRID / 2 - 0 },
	{ 0, VGRID / 2 + 1, HGRID - 1, VGRID / 2 + 1 },
	{ -1 }
};

VLines	nw_e_layout[] = {
	{ 1, 0, HGRID / 2, VGRID / 2 - 1 },
	{ 0, 0, HGRID / 2, VGRID / 2 - 0 },
	{ 0, 1, HGRID / 2, VGRID / 2 + 1 },
	/*{ HGRID / 2, VGRID / 2 - 1, HGRID - 1, VGRID / 2 - 1 },*/
	{ HGRID / 2, VGRID / 2 - 0, HGRID - 1, VGRID / 2 - 0 },
	{ HGRID / 2, VGRID / 2 + 1, HGRID - 1, VGRID / 2 + 1 },
	{ -1 }
};

VLines	sw_e_layout[] = {
	{ 0, VGRID - 2, HGRID / 2 - 1, VGRID / 2 /*- 1*/ },
	{ 0, VGRID - 1, HGRID / 2, VGRID / 2 - 0 },
	{ 1, VGRID - 1, HGRID / 2, VGRID / 2 + 1 },
	/*{ HGRID / 2, VGRID / 2 - 1, HGRID - 1, VGRID / 2 - 1 },*/
	{ HGRID / 2, VGRID / 2 - 0, HGRID - 1, VGRID / 2 - 0 },
	{ HGRID / 2, VGRID / 2 + 1, HGRID - 1, VGRID / 2 + 1 },
	{ -1 }
};

VLines	w_ne_layout[] = {
	/*{ 0, VGRID / 2 - 1, HGRID / 2, VGRID / 2 - 1 },*/
	{ 0, VGRID / 2 - 0, HGRID / 2, VGRID / 2 - 0 },
	{ 0, VGRID / 2 + 1, HGRID / 2, VGRID / 2 + 1 },
	{ HGRID / 2, VGRID / 2 - 1, HGRID - 2, 0 },
	{ HGRID / 2, VGRID / 2 - 0, HGRID - 1, 0 },
	{ HGRID / 2, VGRID / 2 + 1, HGRID - 1, 1 },
	{ -1 }
};

VLines	w_se_layout[] = {
	/*{ 0, VGRID / 2 - 1, HGRID / 2 - 0, VGRID / 2 - 1 },*/
	{ 0, VGRID / 2 - 0, HGRID / 2, VGRID / 2 - 0 },
	{ 0, VGRID / 2 + 1, HGRID / 2, VGRID / 2 + 1 },
	{ HGRID / 2 + 1, VGRID / 2 /*- 1*/, HGRID - 1, VGRID - 2 },
	{ HGRID / 2, VGRID / 2 - 0, HGRID - 1, VGRID - 1 },
	{ HGRID / 2, VGRID / 2 + 1, HGRID - 2, VGRID - 1 },
	{ -1 }
};

VLines	sweng_sw_ne_straight[] = {
	{ 0, VGRID - 2, HGRID - 2, 0 },
	{ 0, VGRID - 1, HGRID - 1, 0 },
	{ 1, VGRID - 1, HGRID - 1, 1 },

	{ 0, VGRID / 2, HGRID / 2 - 1, VGRID / 2 },
	{ 0, VGRID / 2 + 1, HGRID / 2 - 1, VGRID / 2 + 1 },

	{ HGRID / 2 + 1, VGRID / 2 + 1, HGRID - 1, VGRID / 2 + 1 },
	{ HGRID / 2 + 1, VGRID / 2 - 0, HGRID - 1, VGRID / 2 - 0 },
	{ -1 }
};

VLines	sweng_sw_ne_switched[] = {

	{ 0, VGRID / 2, HGRID - 2, 0 },
	{ 0, VGRID / 2 + 1, HGRID - 1, 0 },

	{ 0, VGRID - 1, HGRID - 1, VGRID / 2 },
	{ 1, VGRID - 1, HGRID - 1, VGRID / 2 + 1 },
	{ -1 }
};

VLines	sweng_nw_se_straight[] = {
	{ 1, 0, HGRID - 1, VGRID - 2 },
	{ 0, 0, HGRID - 1, VGRID - 1 },
	{ 0, 1, HGRID - 2, VGRID - 1 },

	{ 0, VGRID / 2, HGRID / 2 - 1, VGRID / 2 },
	{ 0, VGRID / 2 + 1, HGRID / 2 - 1, VGRID / 2 + 1 },

	{ HGRID / 2 + 1, VGRID / 2 + 1, HGRID - 1, VGRID / 2 + 1 },
	{ HGRID / 2 + 1, VGRID / 2 - 0, HGRID - 1, VGRID / 2 - 0 },
	{ -1 }
};

VLines	sweng_nw_se_switched[] = {

	{ 0, 0, HGRID - 1, VGRID / 2 },
	{ 0, 1, HGRID - 1, VGRID / 2 + 1 },

	{ 0, VGRID / 2, HGRID - 1, VGRID - 2 },
	{ 1, VGRID / 2 + 1, HGRID - 1, VGRID - 1 },
	{ -1 }
};

VLines	swengv_sw_ne_straight[] = {
	{ HGRID / 2 + 1, 0, HGRID / 2 + 1, VGRID - 1 },
	{ HGRID / 2 - 0, 0, HGRID / 2 - 0, VGRID - 1 },

	{ 0, VGRID - 2, HGRID - 2, 0 },
	{ 0, VGRID - 1, HGRID - 1, 0 },
	{ 1, VGRID - 1, HGRID - 1, 1 },

	{ -1 }
};

VLines	swengv_sw_ne_switched[] = {

	{ 0, VGRID - 2, HGRID / 2 - 0, 0 },
	{ 0, VGRID - 1, HGRID / 2 + 1, 0 },

	{ HGRID / 2 - 0, VGRID - 1, HGRID - 1, 0 },
	{ HGRID / 2 + 1, VGRID - 1, HGRID - 1, 1 },
	{ -1 }
};

VLines	swengv_nw_se_straight[] = {
	{ HGRID / 2 + 1, 0, HGRID / 2 + 1, VGRID - 1 },
	{ HGRID / 2 - 0, 0, HGRID / 2 - 0, VGRID - 1 },

	{ 1, 0, HGRID - 1, VGRID - 2 },
	{ 0, 0, HGRID - 1, VGRID - 1 },
	{ 0, 1, HGRID - 2, VGRID - 1 },

	{ -1 }
};

VLines	swengv_nw_se_switched[] = {

	{ 0, 0, HGRID / 2 - 1, VGRID - 1 },
	{ 0, 1, HGRID / 2 - 0, VGRID - 1 },

	{ HGRID / 2 - 0, 0, HGRID - 1, VGRID - 2 },
	{ HGRID / 2 + 1, 0, HGRID - 1, VGRID - 1 },
	{ -1 }
};

VLines	block_layout[] = {
	{ HGRID / 2, VGRID / 2 - 1, HGRID / 2, VGRID / 2 + 2 },
	{ -1 }
};

VLines	block_layout_ns[] = {
	{ HGRID / 2 - 1, VGRID / 2, HGRID / 2 + 2, VGRID / 2 },
	{ -1 }
};

VLines	nw_se_layout[] = {
	{ 1, 0, HGRID - 1, VGRID - 2 },
	{ 0, 0, HGRID - 1, VGRID - 1 },
	{ 0, 1, HGRID - 2, VGRID - 1 },
	{ -1 }
};

VLines	sw_ne_layout[] = {
	{ 0, VGRID - 2, HGRID - 2, 0 },
	{ 0, VGRID - 1, HGRID - 1, 0 },
	{ 1, VGRID - 1, HGRID - 1, 1 },
	{ -1 }
};

VLines	switch_rect[] = {
	{ 0, 0, HGRID - 1, 0 },
	{ HGRID - 1, 0, HGRID - 1, VGRID - 1 },
	{ 0, 0, 0, VGRID - 1 },
	{ 0, VGRID - 1, HGRID - 1, VGRID - 1 },
	{ -1 }
};

VLines	w_e_platform_out[] = {
	{ 0, VGRID / 2 - 3, HGRID - 1, VGRID / 2 - 3 },
	{ 0, VGRID / 2 + 3, HGRID - 1, VGRID / 2 + 3 },
	{ 0, VGRID / 2 - 3, 0, VGRID / 2 + 3 },
	{ HGRID - 1, VGRID / 2 - 3, HGRID - 1, VGRID / 2 + 3 },
	{ -1 }
};

VLines	w_e_platform_in[] = {
	{ 1, VGRID / 2 - 2, HGRID - 2, VGRID / 2 - 2 },
	{ 1, VGRID / 2 - 1, HGRID - 2, VGRID / 2 - 1 },
	{ 1, VGRID / 2 - 0, HGRID - 2, VGRID / 2 - 0 },
	{ 1, VGRID / 2 + 1, HGRID - 2, VGRID / 2 + 1 },
	{ 1, VGRID / 2 + 2, HGRID - 2, VGRID / 2 + 2 },
	{ -1 }
};

VLines	n_s_platform_out[] = {
	{ HGRID / 2 - 3, 0, HGRID / 2 - 3, VGRID - 1 },
	{ HGRID / 2 + 3, 0, HGRID / 2 + 3, VGRID - 1 },
	{ HGRID / 2 - 3, 0, HGRID / 2 + 3, 0 },
	{ HGRID / 2 - 3, VGRID - 1, HGRID / 2 + 3, VGRID - 1 },
	{ -1 }
};

VLines	n_s_platform_in[] = {
	{ HGRID / 2 - 2, 1, HGRID / 2 - 2, VGRID - 2 },
	{ HGRID / 2 - 1, 1, HGRID / 2 - 1, VGRID - 2 },
	{ HGRID / 2 - 0, 1, HGRID / 2 - 0, VGRID - 2 },
	{ HGRID / 2 + 1, 1, HGRID / 2 + 1, VGRID - 2 },
	{ HGRID / 2 + 2, 1, HGRID / 2 + 2, VGRID - 2 },
	{ -1 }
};

VLines	itin_layout[] = {
	{ 0, 0, HGRID - 1, VGRID - 1 },
	{ 0, VGRID / 2, HGRID - 1, VGRID / 2 },
	{ 0, VGRID - 1, HGRID - 1, 0 },
	{ -1 }
};

VLines  etrigger_layout[] = {
	{ 1, 2, HGRID - 2, 2 },
	{ 1, 2, HGRID / 2, VGRID - 2 },
	{ HGRID / 2, VGRID - 2, HGRID - 2, 2 },
	{ -1 }
};

VLines  wtrigger_layout[] = {
	{ 1, VGRID - 2, HGRID - 2, VGRID - 2 },
	{ 1, VGRID - 2, HGRID / 2, 2 },
	{ HGRID / 2, 2, HGRID - 2, VGRID - 2 },
	{ -1 }
};

VLines  ntrigger_layout[] = {
	{ 2, 1, 2, VGRID - 2 },
	{ 2, 1, HGRID - 2, VGRID / 2 },
	{ 2, VGRID - 2, HGRID - 2, VGRID / 2 },
	{ -1 }
};

VLines  strigger_layout[] = {
	{ HGRID - 2, 1, HGRID - 2, VGRID - 2 },
	{ 2, VGRID / 2, HGRID - 2, 1 },
	{ 2, VGRID / 2, HGRID - 2, VGRID - 2 },
	{ -1 }
};

#if 0
VLines	w_link[] = {
	{ 0, VGRID / 2, HGRID / 2, VGRID / 2 },
	{ HGRID / 2, VGRID / 2, HGRID / 2 + 4, VGRID / 2 - 4 },
	{ HGRID / 2, VGRID / 2, HGRID / 2 + 4, VGRID / 2 + 4 },
	{ -1 }
};

VLines	e_link[] = {
	{ HGRID / 2 - 4, VGRID / 2 - 4, HGRID / 2, VGRID / 2 },
	{ HGRID / 2 - 4, VGRID / 2 + 4, HGRID / 2, VGRID / 2 },
	{ HGRID / 2, VGRID / 2, HGRID - 1, VGRID / 2 },
	{ -1 }
};

#endif

void	    *e_train_pmap_default[NTTYPES];
void	    *w_train_pmap_default[NTTYPES];
void	    *e_car_pmap_default[NTTYPES];
void	    *w_car_pmap_default[NTTYPES];

void	*e_train_pmap[NTTYPES];
static const char * e_train_xpm[] = {
"13 10 3 1",
"       c #FFFFFFFFFFFF",
".      c #000000000000",
NULL, /*"X      c #0000FFFFFFFF",*/
"             ",
"...........  ",
".XXXXXXXXX.. ",
".X..X..X..X..",
".XXXXXXXXXXX.",
".XXXXXXXXXXX.",
".............",
"  ...   ...  ",
"             ",
"             "};

void	*w_train_pmap[NTTYPES];
static const char * w_train_xpm[] = {
"13 10 3 1",
"       c #FFFFFFFFFFFF",
".      c #000000000000",
NULL, /*"X      c #0000FFFFFFFF",*/
"             ",
"  ...........",
" ..XXXXXXXXX.",
"..X.X..X..XX.",
".XXXXXXXXXXX.",
".XXXXXXXXXXX.",
".............",
"  ...   ...  ",
"             ",
"             "};

void	*w_car_pmap[NTTYPES];
void	*e_car_pmap[NTTYPES];
static	const char * car_xpm[] = {	/* same for both e and w */
"13 10 3 1",
"       c #FFFFFFFFFFFF",
".      c #000000000000",
NULL, /*"X      c #0000FFFFFFFF",*/
"             ",
"............ ",
".XXXXXXXXXX. ",
".X..X..X..X. ",
".XXXXXXXXXX. ",
"XXXXXXXXXXXX ",
"............ ",
" ...    ...  ",
"             ",
"             "};

static	void	*speed_pmap;
static	const char *speed_xpm[] = {
"8 3 3 1",
"       c #FFFFFFFFFFFF",
".      c #000000000000",
"X      c #000000000000",
"  ....  ",
" ..  .. ",
"  ....  "};

static	void	*camera_pmap;
static	const char *camera_xpm[] = {
"13 10 3 1",
"       c #FFFFFFFFFFFF",
".      c #000000000000",
"X      c #0000FFFFFFFF",
"             ",
"   ..        ",
" ........... ",
" . ..      . ",
" .   ...   . ",
" .   . .   . ",
" .   ...   . ",
" .         . ",
" ........... ",
"             "};

static	void	*itin_pmap;
static	const char *itin_xpm[] = {
"8 9 4 1",
"       c lightgray",
".      c #000000000000",
"X      c gray",
"#      c black",
"        ",
"  ....  ",
" ...... ",
"..XXXX..",
".XXXXXX.",
"..XXXX..",
"#......#",
" #....# ",
"  ####  "
};


static	const char	*ttypecolors[NTTYPES] = {
	"orange", "cyan", "blue", "yellow",
	"white", "red", "brown", "green"
};


/*
 *	Tools-types pixmaps
 *	(created from xpms defined in the i*.h files)
 */

void	*tracks_pixmap, *switches_pixmap, *signals_pixmap,
	*tools_pixmap, *actions_pixmap,
	*move_start_pixmap, *move_end_pixmap, *move_dest_pixmap;

static	char	buff[256];

void	init_pmaps(void)
{
	int	r, g, b;
	int	fgr, fgg, fgb;
	int	i;
	char	bufffg[64];
	char	buffcol[64];

	getcolor_rgb(fieldcolors[COL_TRACK], &fgr, &fgg, &fgb);
	sprintf(bufffg, ".      c #%02x00%02x00%02x00", fgr, fgg, fgb);
	getcolor_rgb(fieldcolors[COL_BACKGROUND], &r, &g, &b);
	sprintf(buff, "       c #%02x00%02x00%02x00", r, g, b);
	sprintf(buff, "       c lightgray", r, g, b);

	e_train_xpm[1] = w_train_xpm[1] = car_xpm[1] = buff;
	e_train_xpm[2] = w_train_xpm[2] = car_xpm[2] = bufffg;
	e_train_xpm[3] = w_train_xpm[3] = car_xpm[3] = buffcol;

	for(i = 0; i < NTTYPES; ++i) {
	    sprintf(buffcol, "X      c %s", ttypecolors[i]);
	    e_train_pmap[i] = get_pixmap(e_train_xpm);
	    w_train_pmap[i] = get_pixmap(w_train_xpm);
	    e_car_pmap[i] = get_pixmap(car_xpm);
	    w_car_pmap[i] = get_pixmap(car_xpm);
	}

	Signal::InitPixmaps();

	sprintf(buff, "       c #%02x00%02x00%02x00", r, g, b);
	sprintf(bufffg, ".      c #%02x00%02x00%02x00", fgr, fgg, fgb);
	speed_xpm[1] = buff;
	speed_xpm[2] = bufffg;
	speed_pmap = get_pixmap(speed_xpm);

	for(r = 0; r < 4; ++r) {
	    e_train_pmap_default[r] = e_train_pmap[r];
	    w_train_pmap_default[r] = w_train_pmap[r];
	    e_car_pmap_default[r] = e_car_pmap[r];
	    w_car_pmap_default[r] = w_car_pmap[r];
	}

	// tools-types pixmaps

	tracks_pixmap = get_pixmap(tracks_xpm);
	switches_pixmap = get_pixmap(switches_xpm);
	signals_pixmap = get_pixmap(signals_xpm);
	tools_pixmap = get_pixmap(tools_xpm);
	actions_pixmap = get_pixmap(actions_xpm);
	move_start_pixmap = get_pixmap(move_start_xpm);
	move_end_pixmap = get_pixmap(move_end_xpm);
	move_dest_pixmap = get_pixmap(move_dest_xpm);
}

void	free_pixmaps(void)
{
	int	    i;

	for(i = 0; i < 4; ++i) {
	    delete_pixmap(e_train_pmap[i]);
	    delete_pixmap(w_train_pmap[i]);
	    delete_pixmap(e_car_pmap[i]);
	    delete_pixmap(w_car_pmap[i]);
	}

	Signal::FreePixmaps();
	delete_pixmap(tracks_pixmap);
	delete_pixmap(switches_pixmap);
	delete_pixmap(signals_pixmap);
	delete_pixmap(tools_pixmap);
	delete_pixmap(actions_pixmap);
	delete_pixmap(move_start_pixmap);
	delete_pixmap(move_end_pixmap);
	delete_pixmap(move_dest_pixmap);
	delete_pixmap(speed_pmap);
}

Track	*track_new(void)
{
	Track	*t;

//	t = (Track *)malloc(sizeof(Track));
//	memset(t, 0, sizeof(Track));
	t = new Track();
	t->xsize = 1;
	t->ysize = 1;
	t->type = NOTRACK;
	t->direction = NODIR;
	t->fgcolor = fieldcolors[COL_TRACK];
	return(t);
}

void	track_delete(Track *t)
{
	Track	*t1, *old;

	if(t == layout)
	    layout = t->next;
	else {
	    old = layout;
	    for(t1 = old->next; t1 != t; t1 = t1->next)
		old = t1;
	    old->next = t->next;
	}
	if(t->station)
	    free(t->station);
	delete_script_data(t);
	//free(t);
	delete t;
	link_all_tracks();
}

void	track_name(Track *t, wxChar *name)
{
	if(t->station)
	    free(t->station);
	t->station = wxStrdup(name);
}

void	track_draw(Track *t)
{
	int	fg;
	int	tot;
	VLines	*lns = n_s_layout;	// provide dummy initialization - always overwritten

	fg = t->fgcolor;
	switch(t->status) {
	case ST_FREE:
		break;
	case ST_BUSY:
		fg = color_red;
		break;
	case ST_READY:
		fg = color_green;
		break;
	case ST_WORK:
		fg = color_blue;
	}
	switch(t->direction) {

	case TRK_N_S:
		lns = n_s_layout;
		break;

	case SW_N:
		lns = sw_n_layout;
		break;

	case NW_S:
		lns = nw_s_layout;
		break;

	case W_E:
		lns = w_e_layout;
		break;

	case NW_E:
		lns = nw_e_layout;
		break;

	case SW_E:
		lns = sw_e_layout;
		break;

	case W_NE:
		lns = w_ne_layout;
		break;

	case W_SE:
		lns = w_se_layout;
		break;

	case NW_SE:
		lns = nw_se_layout;
		break;

	case SW_NE:
		lns = sw_ne_layout;
		break;

	case NE_S:
		lns = ne_s_layout;
		break;

	case SE_N:
		lns = se_n_layout;
		break;

	case XH_NW_SE:
		fg = t->direction;
		t->direction = NW_SE;
		track_draw(t);
		t->direction = W_E;
		track_draw(t);
		t->direction = (trkdir)fg;
		return;

	case XH_SW_NE:
		fg = t->direction;
		t->direction = SW_NE;
		track_draw(t);
		t->direction = W_E;
		track_draw(t);
		t->direction = (trkdir)fg;
		return;

	case X_X:
		fg = t->direction;
		t->direction = SW_NE;
		track_draw(t);
		t->direction = NW_SE;
		track_draw(t);
		t->direction = (trkdir)fg;
		return;

	case X_PLUS:
		fg = t->direction;
		t->direction = TRK_N_S;
		track_draw(t);
		t->direction = W_E;
		track_draw(t);
		t->direction = (trkdir)fg;
		return;
	}
	draw_layout(t->x, t->y, lns, fg);
	if(show_blocks && t->direction == W_E && t->length >= 100)
	    draw_layout(t->x, t->y, block_layout, fieldcolors[TRACK]);
	if(show_blocks && t->direction == TRK_N_S && t->length >= 100)
	    draw_layout(t->x, t->y, block_layout_ns, fieldcolors[TRACK]);
	if(editing && show_links) {
	    if(t->wlinkx && t->wlinky)
		draw_link(t->x, t->y, t->wlinkx, t->wlinky, conf.linkcolor2);
	    if(t->elinkx && t->elinky)
		draw_link(t->x, t->y, t->elinkx, t->elinky, conf.linkcolor2);
	}
	if(!show_speeds)
	    return;
	tot = 0;
	for(fg = 0; fg < NTTYPES; ++fg)
	    tot += t->speed[fg];
	if(tot)
	    draw_pixmap(t->x, t->y, speed_pmap);
}

void	switch_draw(Track *t)
{
	int	fg;
	int	tmp;

	fg = t->fgcolor;
	switch(t->status) {
	case ST_FREE:
		break;
	case ST_BUSY:
		fg = color_red;
		break;
	case ST_READY:
		fg = color_green;
		break;
	case ST_WORK:
		fg = color_blue;
	}
	tmp = t->direction;
	switch(tmp) {
	case 0:
		if(editing) {
		    t->direction = W_NE;
		    track_draw(t);
		    t->direction = W_E;
		    track_draw(t);
		} else if(t->switched) {
		    t->direction = W_NE;
		    track_draw(t);
		} else
		    t->direction = W_E;
		    track_draw(t);
		break;

	case 1:
		if(editing) {
		    t->direction = NW_E;
		    track_draw(t);
		    t->direction = W_E;
		    track_draw(t);
		} else if(t->switched) {
		    t->direction = NW_E;
		    track_draw(t);
		} else
		    t->direction = W_E;
		    track_draw(t);
		break;

	case 2:
		if(editing) {
		    t->direction = W_SE;
		    track_draw(t);
		    t->direction = W_E;
		    track_draw(t);
		} else if(t->switched) {
		    t->direction = W_SE;
		    track_draw(t);
		} else
		    t->direction = W_E;
		    track_draw(t);
		break;

	case 3:
		if(editing) {
		    t->direction = SW_E;
		    track_draw(t);
		    t->direction = W_E;
		    track_draw(t);
		} else if(t->switched) {
		    t->direction = SW_E;
		    track_draw(t);
		} else
		    t->direction = W_E;
		    track_draw(t);
		break;

	case 4:
		if(editing) {
		    t->direction = SW_E;
		    track_draw(t);
		    t->direction = SW_NE;
		} else if(t->switched)
		    t->direction = SW_E;
		else
		    t->direction = SW_NE;
		track_draw(t);
		break;

	case 5:
		if(editing) {
		    t->direction = W_NE;
		    track_draw(t);
		    t->direction = SW_NE;
		} else if(t->switched)
		    t->direction = W_NE;
		else
		    t->direction = SW_NE;
		track_draw(t);
		break;

	case 6:
		if(editing) {
		    t->direction = NW_E;
		    track_draw(t);
		    t->direction = NW_SE;
		} else if(t->switched) {
		    t->direction = NW_E;
		} else
		    t->direction = NW_SE;
		track_draw(t);
		break;

	case 7:
		if(editing) {
		    t->direction = W_SE;
		    track_draw(t);
		    t->direction = NW_SE;
		} else if(t->switched)
		    t->direction = W_SE;
		else
		    t->direction = NW_SE;
		track_draw(t);
		break;

	case 8:				/* horizontal english switch */
		if(t->switched && !editing)
		    draw_layout(t->x, t->y, sweng_sw_ne_switched, fg);
		else
		    draw_layout(t->x, t->y, sweng_sw_ne_straight, fg);
		break;
		    
	case 9:				/* horizontal english switch */
		if(t->switched && !editing)
		    draw_layout(t->x, t->y, sweng_nw_se_switched, fg);
		else
		    draw_layout(t->x, t->y, sweng_nw_se_straight, fg);
		break;

	case 10:
		if(editing) {
		    t->direction = W_SE;
		    track_draw(t);
		    t->direction = W_NE;
		} else if (t->switched)
		    t->direction = W_SE;
		else
		    t->direction = W_NE;
		track_draw(t);
		break;

	case 11:
		if(editing) {
		    t->direction = SW_E;
		    track_draw(t);
		    t->direction = NW_E;
		} else if (t->switched)
		    t->direction = SW_E;
		else
		    t->direction = NW_E;
		track_draw(t);
		break;

	case 12:
		if(editing) {
		    t->direction = TRK_N_S;
		    track_draw(t);
		    t->direction = SW_N;
		} else if(t->switched)
		    t->direction = SW_N;
		else
		    t->direction = TRK_N_S;
		track_draw(t);
		break;

	case 13:
		if(editing) {
		    t->direction = TRK_N_S;
		    track_draw(t);
		    t->direction = SE_N;
		} else if(t->switched)
		    t->direction = SE_N;
		else
		    t->direction = TRK_N_S;
		track_draw(t);
		break;

	case 14:
		if(editing) {
		    t->direction = TRK_N_S;
		    track_draw(t);
		    t->direction = NW_S;
		} else if(t->switched)
		    t->direction = NW_S;
		else
		    t->direction = TRK_N_S;
		track_draw(t);
		break;

	case 15:
		if(editing) {
		    t->direction = TRK_N_S;
		    track_draw(t);
		    t->direction = NE_S;
		} else if(t->switched)
		    t->direction = NE_S;
		else
		    t->direction = TRK_N_S;
		track_draw(t);
		break;

	case 16:			/* vertical english switch */
		if(t->switched && !editing)
		    draw_layout(t->x, t->y, swengv_sw_ne_switched, fg);
		else
		    draw_layout(t->x, t->y, swengv_sw_ne_straight, fg);
		break;
		    
	case 17:			/* vertical english switch */
		if(t->switched && !editing)
		    draw_layout(t->x, t->y, swengv_nw_se_switched, fg);
		else
		    draw_layout(t->x, t->y, swengv_nw_se_straight, fg);
		break;

	case 18:
		if(editing) {
		    t->direction = SW_NE;
		    track_draw(t);
		    t->direction = SW_N;
		} else if(t->switched)
		    t->direction = SW_N;
		else
		    t->direction = SW_NE;
		track_draw(t);
		break;

	case 19:
		if(editing) {
		    t->direction = SW_NE;
		    track_draw(t);
		    t->direction = NE_S;
		} else if(t->switched)
		    t->direction = NE_S;
		else
		    t->direction = SW_NE;
		track_draw(t);
		break;

	case 20:
		if(editing) {
		    t->direction = NW_SE;
		    track_draw(t);
		    t->direction = SE_N;
		} else if(t->switched)
		    t->direction = SE_N;
		else
		    t->direction = NW_SE;
		track_draw(t);
		break;

	case 21:
		if(editing) {
		    t->direction = NW_SE;
		    track_draw(t);
		    t->direction = NW_S;
		} else if(t->switched)
		    t->direction = NW_S;
		else
		    t->direction = NW_SE;
		track_draw(t);
		break;

	case 22:
		if(editing) {
		    t->direction = NW_S;
		    track_draw(t);
		    t->direction = NE_S;
		} else if(t->switched)
		    t->direction = NW_S;
		else
		    t->direction = NE_S;
		track_draw(t);
		break;

	case 23:
		if(editing) {
		    t->direction = SW_N;
		    track_draw(t);
		    t->direction = SE_N;
		} else if(t->switched)
		    t->direction = SW_N;
		else
		    t->direction = SE_N;
		track_draw(t);
		break;
	}
	if(!t->norect)
	    draw_layout(t->x, t->y, switch_rect, fieldcolors[TRACK]);
	t->direction = (trkdir)tmp;
}

void	platform_draw(Track *t)
{
	switch(t->direction) {
	case W_E:
		draw_layout(t->x, t->y, w_e_platform_out, fieldcolors[TRACK]);
		draw_layout(t->x, t->y, w_e_platform_in, color_darkgray);
		break;

	case N_S:
		draw_layout(t->x, t->y, n_s_platform_out, fieldcolors[TRACK]);
		draw_layout(t->x, t->y, n_s_platform_in, color_darkgray);
		break;
	}
}

void	signal_draw(Track *t)
{
	Signal	*signal = (Signal *)t;
	signal->Draw();
}

void	train_draw(Track *t, Train *trn)
{
	void	*pixels;

	if(!e_train_pmap[0]) {
	    init_pmaps();
	}
        if(trn->direction == W_E)
	    pixels = trn->epix == -1 ?
			e_train_pmap[trn->type] : pixmaps[trn->epix].pixels;
	else
	    pixels = trn->wpix == -1 ?
			w_train_pmap[trn->type] : pixmaps[trn->wpix].pixels;
	draw_pixmap(t->x, t->y, pixels);
}

void	car_draw(Track *t, Train *trn)
{
	void	*pixels;

	if(!e_car_pmap[0]) {
	    init_pmaps();
	}
	if(trn->direction == W_E)
	    pixels = trn->ecarpix == -1 || trn->ecarpix >= ncarpixmaps ?
			e_car_pmap[trn->type] : carpixmaps[trn->ecarpix].pixels;
	else
	    pixels = trn->wcarpix == -1 || trn->wcarpix >= ncarpixmaps ?
			w_car_pmap[trn->type] : carpixmaps[trn->wcarpix].pixels;
	draw_pixmap(t->x, t->y, pixels);
}

void	text_draw(Track *t)
{
	if(!t->station)
	    return;
	tr_fillrect(t->x, t->y);
	if(t->_fontIndex)
	    draw_layout_text_font(t->x, t->y, t->station, t->_fontIndex);
	else
	    draw_layout_text1(t->x, t->y, t->station, t->direction);
	if(!editing || !show_links)
	    return;
	if(t->elinkx && t->elinky)
	    draw_link(t->x, t->y, t->elinkx, t->elinky, conf.linkcolor);
	else if(t->wlinkx && t->wlinky)
	    draw_link(t->x, t->y, t->wlinkx, t->wlinky, conf.linkcolor);
}

void	link_draw(Track *t)
{
	tr_fillrect(t->x, t->y);
	if(t->direction == W_E)
	    draw_layout_text1(t->x, t->y, wxT("...to..."), 1);
	else
	    draw_layout_text1(t->x, t->y, wxT("Link..."), 1);
}

void	macro_draw(Track *t)
{
	tr_fillrect(t->x, t->y);
	if(t->direction == 0)
	    draw_layout_text1(t->x, t->y, wxT("Macro"), 1);
	else
	    draw_layout_text1(t->x, t->y, wxT("Place"), 1);
}

void	itin_draw(Track *t)
{
	if(!itin_pmap)
	    itin_pmap = get_pixmap(itin_xpm);

	tr_fillrect(t->x, t->y);
	draw_pixmap(t->x, t->y, itin_pmap);

	if(t->station) {
#if 0 // !Rask Ingemann Lambertsen
	    draw_itin_text(t->x, t->y, t->station, t->direction == 1);
#else
	    wxChar *label = wxStrrchr(t->station, '@');
	    if(label)
		label++;
	    else
		label = t->station;
	    draw_itin_text(t->x, t->y, label, t->direction == 1);
#endif
	}
}

void	mover_draw()
{
	draw_link(move_start.x, move_start.y, move_end.x, move_end.y, color_white);
}

void	trigger_draw(Track *t)
{
	VLines	*img;

	switch(t->direction) {
	case S_N:
	    img = ntrigger_layout;
	    break;
	case N_S:
	    img = strigger_layout;
	    break;
	case W_E:
	    img = etrigger_layout;
	    break;
	case E_W:
	    img = wtrigger_layout;
	    break;
	default:
	    return;
	}

	draw_layout(t->x, t->y, img, color_blue);
	if(editing && show_links) {
	    if(t->wlinkx && t->wlinky)
		draw_link(t->x, t->y, t->wlinkx, t->wlinky, conf.linkcolor);
	}
}

void	image_draw(Track *t)
{
	wxChar	buff[256];
	wxChar	*p;

	if(!camera_pmap)
	    camera_pmap = get_pixmap(camera_xpm);
	if(t->direction || !t->station || !*t->station) {/* filename! */
	    if(!t->pixels)
		t->pixels = camera_pmap;
	} else if(!t->pixels) {
	    t->pixels = get_pixmap_file(t->station);
	    if(!t->pixels) {	    /* for UNIX, try lower case name */
		wxStrcpy(buff, t->station);
		for(p = buff; *p; ++p)
		    if(*p >= 'A' && *p <= 'Z')
			*p += ' ';
		t->pixels = get_pixmap_file(buff);
	    }
	    if(!t->pixels) {
		wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s '%s'."), L("Error reading"), t->station);
		do_alert(buff);
		t->pixels = camera_pmap;
	    }
	}
	draw_pixmap(t->x, t->y, t->pixels);
}

void	track_paint(Track *t)
{
	tr_fillrect(t->x, t->y);
	if(!editing && t->invisible)
	    return;

	switch(t->type) {
	case TRACK:
		track_draw(t);
		break;

	case SWITCH:
		switch_draw(t);
		break;

	case PLATFORM:
		platform_draw(t);
		break;

	case TSIGNAL:
		signal_draw(t);
		break;

	case TRAIN:		/* trains are handled differently */
	/*	train_draw(t); */
		break;

	case TEXT:
		text_draw(t);
		break;

	case LINK:
		link_draw(t);
		break;

	case IMAGE:
		image_draw(t);
		break;

	case MACRO:
		macro_draw(t);
		break;

	case ITIN:
		itin_draw(t);
		break;

	case TRIGGER:
		trigger_draw(t);
		break;

	default:
		return;
	}
}

const Char *GetColorName(int color)
{
	if(color == conf.fgcolor)
	    return wxT("black");
	if(color == color_white)
	    return wxT("white");
	if(color == color_orange)
	    return wxT("orange");
	if(color == color_green)
	    return wxT("green");
	if(color == color_red)
	    return wxT("red");
	return wxT("unknown");
}

const wxChar	*train_next_stop(Train *t, int *final)
{
	Track   *tr;
	static	wxChar	buff[256];
	TrainStop   *ts, *last;

	*final = 0;
	buff[0] = 0;
	buff[1] = 0;
	if(t->status != train_RUNNING && t->status != train_WAITING &&
	    t->status != train_STOPPED)
	    return buff;
	buff[0] = 0;
	last = 0;
	for(ts = t->stops; ts; ts = ts->next) {
	    if(!ts->minstop)
		continue;
	    if(!(tr = findStationNamed(ts->station)) || tr->type != TRACK)
		continue;
	    if(ts->stopped)
		continue;
	    if(!last || ts->arrival < last->arrival)
		last = ts;
	}
	if(!last) {
	    tr = findStationNamed(t->exit);
	    if(!tr || tr->type == TEXT)
		return buff;
	    *final = 1;
	    wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT(" %s %s %s %s   "), L("Final stop"), t->exit, L("at"), format_time(t->timeout));
	} else
	    wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT(" %s %s %s %s   "), L("Next stop"), last->station, L("at"), format_time(last->arrival));
	return buff;
}

bool	is_canceled(Train *t)
{
	if(!t->days || !run_day || (t->days & run_day))
	    return false;
	return true;
}

const wxChar	*train_status0(Train *t, int full)
{
	static	wxChar	buff[256];
	int	i, j, k, final;

	if(terse_status)
	    full = 0;
	buff[0] = 0;
	i = 0;
	switch(t->status) {
	case train_READY:
		if(!is_canceled(t))
		    return L("ready");
		wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s "), L("Canceled - runs on"));
		k = wxStrlen(buff);
		for(i = 1, j = '1'; i < 0x80; i <<= 1, ++j)
		    if(t->days & i)
			buff[k++] = j;
		buff[k] = 0;
		return buff;

	case train_RUNNING:
		if(full)
		    wxStrcpy(buff, train_next_stop(t, &final));
		if(t->shunting)
		    wxStrcpy(buff + wxStrlen(buff), L("Shunting"));
		else if(full) {
		    if(final)
			wxSnprintf(buff + wxStrlen(buff), sizeof(buff)/sizeof(wxChar) - wxStrlen(buff), wxT("%s: %d Km/h"), L("Speed"), t->curspeed);
		    else
			wxSnprintf(buff + wxStrlen(buff), sizeof(buff)/sizeof(wxChar) - wxStrlen(buff), wxT("%s: %d Km/h %s %s"), L("Speed"),
				    t->curspeed, L("to"), t->exit);
		} else
		    wxSnprintf(buff + wxStrlen(buff), sizeof(buff)/sizeof(wxChar) - wxStrlen(buff), wxT("%s %s"), L("Running. Dest."), t->exit);
		return buff;

	case train_STOPPED:
		if(full) {
		    wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %s "), L("Stopped. ETD"), format_time(t->timedep));
		    if(full)
			wxStrcat(buff, train_next_stop(t, &final));
		    if(!final) {
			wxStrcat(buff, L("Dest"));
			wxStrcat(buff, wxT(" "));
			wxStrcat(buff, t->exit);
		    }
		} else
		    wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %s  %s %s"), L("Stopped. ETD"), format_time(t->timedep),
			    L("Dest."), t->exit);
		return buff;

	case train_DELAY:
		wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %s"), L("Delayed entry at"), t->entrance);
		return buff;

	case train_WAITING:
		wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s. %s%s %s"), L("Waiting"),
			full ? train_next_stop(t, &final) : wxT(""), L("Dest."), t->exit);
		return buff;

	case train_DERAILED:
		return L("derailed");

	case train_ARRIVED:
		if(t->wrongdest)
		    wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %s %s %s"), L("Arrived at"), t->exited, L("instead of"), t->exit);
		else if(t->timeexited / 60 > t->timeout / 60)
		    wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %d %s %s"), L("Arrived"),
			(t->timeexited - t->timeout) / 60, L("min. late at"), t->exit);
		else
		    wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), L("Arrived on time"));
		if(t->stock)
		    wxSnprintf(buff + wxStrlen(buff), sizeof(buff)/sizeof(wxChar) - wxStrlen(buff), wxT(" - %s %s"), L("stock for"), t->stock);
		return buff;
	}
	return wxT("");
}

const wxChar	*train_status(Train *t)
{
	return train_status0(t, 0);
}

void	walk_vertical(Track *trk, Track *t, trkdir *ndir)
{
	if(*ndir == N_S) {
	    if(t->elinkx && t->elinky) {
		trk->x = t->elinkx;
		trk->y = t->elinky;
		return;
	    }
	    trk->x = t->x;
	    trk->y = t->y + 1;
	    return;
	}
	if(t->wlinkx && t->wlinky) {
	    trk->x = t->wlinkx;
	    trk->y = t->wlinky;
	    return;
	}
	trk->x = t->x;
	trk->y = t->y - 1;
}

void	walk_vertical_switch(Track *trk, Track *t, trkdir *ndir)
{
	switch(t->direction) {
	case 12:
		if(*ndir == W_E)
		    *ndir = S_N;
		if(*ndir == S_N) {
		    trk->x = t->x;
		    trk->y = t->y - 1;
		} else if(t->switched) {
		    trk->x = t->x - 1;
		    trk->y = t->y + 1;
		    *ndir = E_W;
		} else {
		    trk->x = t->x;
		    trk->y = t->y + 1;
		}
		break;

	case 13:
		if(*ndir == E_W)
		    *ndir = S_N;
		if(*ndir == S_N) {
		    trk->x = t->x;
		    trk->y = t->y - 1;
		} else if(t->switched) {
		    trk->x = t->x + 1;
		    trk->y = t->y + 1;
		    *ndir = W_E;
		} else {
		    trk->x = t->x;
		    trk->y = t->y + 1;
		}
		break;

	case 14:
		if(*ndir == W_E)
		    *ndir = N_S;
		if(*ndir == N_S) {
		    trk->x = t->x;
		    trk->y = t->y + 1;
		} else if(t->switched) {
		    trk->x = t->x - 1;
		    trk->y = t->y - 1;
		    *ndir = E_W;
		} else {
		    trk->x = t->x;
		    trk->y = t->y - 1;
		}
		break;

	case 15:
		if(*ndir == E_W)
		    *ndir = N_S;
		if(*ndir == N_S) {
		    trk->x = t->x;
		    trk->y = t->y + 1;
		} else if(t->switched) {
		    trk->x = t->x + 1;
		    trk->y = t->y - 1;
		    *ndir = W_E;
		} else {
		    trk->x = t->x;
		    trk->y = t->y - 1;
		}
		break;

	case 18:
		if(t->switched) {
		    if(*ndir == W_E)
			*ndir = S_N;
		    if(*ndir == S_N) {
			trk->x = t->x;
			trk->y = t->y - 1;
		    } else {
			trk->x = t->x - 1;
			trk->y = t->y + 1;
			*ndir = E_W;
		    }
		    break;
		}
		if(*ndir == W_E) {
		    trk->x = t->x + 1;
		    trk->y = t->y - 1;
		} else {
		    trk->x = t->x - 1;
		    trk->y = t->y + 1;
		}
		break;

	case 19:
		if(t->switched) {
		    if(*ndir == E_W)
			*ndir = N_S;
		    if(*ndir == S_N) {
			trk->x = t->x + 1;
			trk->y = t->y - 1;
			*ndir = W_E;
		    } else {
			trk->x = t->x;
			trk->y = t->y + 1;
		    }
		    break;
		}
		if(*ndir == W_E || *ndir == S_N) {
		    trk->x = t->x + 1;
		    trk->y = t->y - 1;
		} else {
		    trk->x = t->x - 1;
		    trk->y = t->y + 1;
		}
		break;

	case 20:
		if(t->switched) {
		    if(*ndir == E_W)
			*ndir = S_N;
		    if(*ndir == N_S) {
			trk->x = t->x + 1;
			trk->y = t->y + 1;
			*ndir = W_E;
		    } else {
			trk->x = t->x;
			trk->y = t->y - 1;
		    }
		    break;
		}
		if(*ndir == W_E) {
		    trk->x = t->x + 1;
		    trk->y = t->y + 1;
		} else {
		    trk->x = t->x - 1;
		    trk->y = t->y - 1;
		}
		break;

	case 21:
		if(t->switched) {
		    if(*ndir == W_E)
			*ndir = N_S;
		    if(*ndir == S_N) {
			trk->x = t->x - 1;
			trk->y = t->y - 1;
			*ndir = E_W;
		    } else {
			trk->x = t->x;
			trk->y = t->y + 1;
		    }
		    break;
		}
		if(*ndir == W_E) {
		    trk->x = t->x + 1;
		    trk->y = t->y + 1;
		} else {
		    trk->x = t->x - 1;
		    trk->y = t->y - 1;
		}
		break;

	case 22:
		if(t->switched) {
		    if(*ndir == S_N) {
			trk->x = t->x - 1;
			trk->y = t->y - 1;
			*ndir = E_W;
			break;
		    }
		} else if(*ndir == S_N) {
		    trk->x = t->x + 1;
		    trk->y = t->y - 1;
		    *ndir = W_E;
		    break;
		}
		trk->x = t->x;
		trk->y = t->y + 1;
		*ndir = N_S;
		break;

	case 23:
		if(t->switched) {
		    if(*ndir == N_S) {
			trk->x = t->x - 1;
			trk->y = t->y + 1;
			*ndir = E_W;
			break;
		    }
		} else if(*ndir == N_S) {
		    trk->x = t->x + 1;
		    trk->y = t->y + 1;
		    *ndir = W_E;
		    break;
		}
		trk->x = t->x;
		trk->y = t->y - 1;
		*ndir = S_N;
		break;
	}	
}

Track	*track_walkeast(Track *t, trkdir *ndir)
{
	static	Track	trk;

	if(t->direction != TRK_N_S && t->elinkx && t->elinky) {
	    trk.x = t->elinkx;
	    trk.y = t->elinky;
	    return &trk;
	}
	trk.x = t->x + 1;
	trk.y = t->y;
	switch(t->direction) {
	case NW_SE:
	case W_SE:
		++trk.y;
		break;
	case SW_NE:
	case W_NE:
		--trk.y;
		break;
	case SW_N:
		if(*ndir == N_S) {
		    trk.x = t->x - 1;
		    trk.y = t->y + 1;
		    *ndir = E_W;
		    break;
		}
		trk.y = t->y - 1;
		trk.x = t->x;
		*ndir = S_N;
		break;
	case NW_S:
		if(*ndir == S_N) {
		    *ndir = E_W;
		    trk.x = t->x - 1;
		    trk.y = t->y - 1;
		    break;
		}
		trk.x = t->x;
		trk.y = t->y + 1;
		*ndir = N_S;
		break;
	case NE_S:
		if(*ndir == S_N) {
		    *ndir = W_E;
		    trk.x = t->x + 1;
		    trk.y = t->y - 1;
		    break;
		}
		trk.x = t->x;
		trk.y = t->y + 1;
		*ndir = N_S;
		break;

	case SE_N:
		if(*ndir == N_S) {
		    trk.x = t->x + 1;
		    trk.y = t->y + 1;
		    *ndir = W_E;
		    break;
		}
		trk.y = t->y - 1;
		trk.x = t->x;
		*ndir = S_N;
		break;

	case TRK_N_S:
		walk_vertical(&trk, t, ndir);
		break;

	default:
		*ndir = W_E;
	}
	return &trk;
}

Track	*track_walkwest(Track *t, trkdir *ndir)
{
	static	Track	trk;

	if(t->direction != TRK_N_S && t->wlinkx && t->wlinky) {
	    trk.x = t->wlinkx;
	    trk.y = t->wlinky;
	    return &trk;
	}
	trk.x = t->x - 1;
	trk.y = t->y;
	switch(t->direction) {
	case SW_N:
		if(*ndir == N_S) {
		    ++trk.y;
		    *ndir = E_W;
		    break;
		}
		*ndir = S_N;
	case SW_NE:
	case SW_E:
		++trk.y;
		break;
	case NW_S:
		if(*ndir == N_S) {
		    trk.x = t->x;
		    trk.y = t->y + 1;
		    break;
		}
		*ndir = E_W;
	case NW_SE:
	case NW_E:
		--trk.y;
		break;
	case NE_S:
		if(*ndir == S_N) {
		    trk.x = t->x + 1;
		    trk.y = t->y - 1;
		    *ndir = W_E;
		    break;
	        }
		*ndir = N_S;
		trk.y = t->y + 1;
		trk.x = t->x;
		break;
	case SE_N:
		if(*ndir == N_S) {
		    trk.x = t->x + 1;
		    trk.y = t->y + 1;
		    *ndir = W_E;
		    break;
		}
		*ndir = S_N;
		trk.x = t->x;
		trk.y = t->y - 1;
		break;
	case TRK_N_S:
		walk_vertical(&trk, t, ndir);
		break;
	
	default:
		*ndir = E_W;
	}
	return &trk;
}

Track	*swtch_walkeast(Track *t, trkdir *ndir)
{
	static	Track	trk;

	trk.x = t->x;
	trk.y = t->y;
	switch(t->direction) {
	case 0:
		++trk.x;
		if(t->switched)
		    --trk.y;
		break;

	case 1:
	case 3:
	case 11:
		++trk.x;
		break;

	case 2:
		++trk.x;
		if(t->switched)
		    ++trk.y;
		break;

	case 4:
		++trk.x;
		if(!t->switched)
		    --trk.y;
		break;

	case 5:
		++trk.x;
		--trk.y;
		break;

	case 6:
		++trk.x;
		if(!t->switched)
		    ++trk.y;
		break;

	case 7:
		++trk.x;
		++trk.y;
		break;

	case 8:		    /* These are special cases handled in findPath() */
	case 9:
	case 16:
	case 17:
		break;

	case 10:
		++trk.x;
		if(t->switched)
		    ++trk.y;
		else
		    --trk.y;
		break;

	case 12:
	case 13:
	case 14:
	case 15:
	case 18:
	case 19:
	case 20:
	case 21:
	case 22:
	case 23:
		walk_vertical_switch(&trk, t, ndir);
		break;

	}
	return &trk;
}

Track	*swtch_walkwest(Track *t, trkdir *ndir)
{
	static	Track	trk;

	trk.x = t->x;
	trk.y = t->y;
	switch(t->direction) {
	case 1:
		--trk.x;
		if(t->switched)
		    --trk.y;
		break;

	case 0:
	case 2:
	case 10:
		--trk.x;
		break;

	case 3:
		--trk.x;
		if(t->switched)
		    ++trk.y;
		break;

	case 4:
		--trk.x;
		++trk.y;
		break;

	case 5:
		--trk.x;
		if(!t->switched)
		    ++trk.y;
		break;

	case 7:
		--trk.x;
		if(!t->switched)
		    --trk.y;
		break;

	case 6:
		--trk.x;
		--trk.y;
		break;

	case 8:		    /* These are special cases handled in findPath() */
	case 9:
	case 16:
	case 17:
		break;

	case 11:
		--trk.x;
		if(t->switched)
		    ++trk.y;
		else
		    --trk.y;
		break;

	case 12:
	case 13:
	case 14:
	case 15:
	case 18:
	case 19:
	case 20:
	case 21:
	case 22:
	case 23:
		walk_vertical_switch(&trk, t, ndir);
	}
	return &trk;
}

void	check_layout_errors(void)
{
	Track	*t, *t1;
	wxChar	buff[256];
	int firsttime = 1;

	for(t = layout; t; t = t->next) {
	    buff[0] = 0;
	    if(t->type == TSIGNAL) {
		if(!t->controls)
		    wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %d,%d %s.\n"), L("Signal at"), t->x, t->y, L("not linked to any track"));
		else switch(t->direction) {
		case E_W:
		case signal_WEST_FLEETED:
		case N_S:
		    if(!t->controls->wsignal)
			wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %d,%d - %s %d,%d.\n"), L("Track at"),
				t->controls->x, t->controls->y,
				L("not controlled by signal at"), t->x, t->y);
		    break;
		case W_E:
		case signal_EAST_FLEETED:
		case S_N:
		    if(!t->controls->esignal)
			wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %d,%d - %s %d,%d.\n"), L("Track at"),
				t->controls->x, t->controls->y,
				L("not controlled by signal at"), t->x, t->y);
		    break;
		}
	    }
	    if(t->type == TRACK) {
		if(t->wlinkx && t->wlinky) {
		    if(!(t1 = findTrack(t->wlinkx, t->wlinky)))
			wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %d,%d %s %d,%d.\n"),
			    L("Track at"), t->x, t->y,
			    L("linked to non-existant track at"), t->wlinkx, t->wlinky);
		    else if(!findTrack(t1->elinkx, t1->elinky) &&
			    !findTrack(t1->wlinkx, t1->wlinky))
			wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %d,%d %s %d,%d.\n"),
			    L("Track at"), t1->x, t1->y,
			    L("not linked back to"), t->x, t->y);
		} else if(t->elinkx && t->elinky) {
		    if(!(t1 = findTrack(t->elinkx, t->elinky)))
			wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %d,%d %s %d,%d.\n"),
			    L("Track at"), t->x, t->y,
			    L("linked to non-existant track at"), t->elinkx, t->elinky);
		    else if(!findTrack(t1->elinkx, t1->elinky) &&
			    !findTrack(t1->wlinkx, t1->wlinky))
			wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %d,%d %s %d,%d.\n"),
			    L("Track at"), t1->x, t1->y,
			    L("not linked back to"), t->x, t->y);
		}

	    }
	    if(t->type == SWITCH) {
		if(t->wlinkx && t->wlinky) {
		    if(!(t1 = findSwitch(t->wlinkx, t->wlinky)))
			wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %d,%d %s %d,%d.\n"),
			    L("Switch at"), t->x, t->y,
			    L("linked to non-existant switch at"), t->wlinkx, t->wlinky);
		    else if(t1->wlinkx != t->x || t1->wlinky != t->y)
			wxSnprintf(buff, sizeof(buff)/sizeof(wxChar), wxT("%s %d,%d %s %d,%d.\n"),
			    L("Switch at"), t1->x, t1->y,
			    L("not linked back to switch at"), t->x, t->y);
		}
	    }
	    if(buff[0]) {
		if(firsttime)
		    traindir->layout_error(L("Checking for errors in layout...\n"));
		firsttime = 0;
		traindir->layout_error(buff);
	    }
	}
	traindir->end_layout_error();
}

void	link_tracks(Track *t, Track *t1)
{
	switch(t->type) {
	case TRACK:
		if(t1->type != TRACK) {
		    traindir->Error(L("Only like tracks can be linked."));
		    return;
		}
		if(t1->direction != W_E && t1->direction != TRK_N_S) {
		    traindir->Error(L("Only horizontal or vertical tracks can be linked automatically.\nTo link other track types, use the track properties dialog."));
		    return;
		}
/*
		if(t->direction != t1->direction) {
		    error(wxT("You can't link horizontal to vertical tracks."));
		    return;
		}
*/		if(t->direction == TRK_N_S) {
		    if(!findTrack(t->x, t->y + 1)) {
			t->elinkx = t1->x;
			t->elinky = t1->y;
		    } else {
			t->wlinkx = t1->x;
			t->wlinky = t1->y;
		    }
		    if(!findTrack(t1->x , t1->y + 1)) {
			t1->elinkx = t->x;
			t1->elinky = t->y;
		    } else {
			t1->wlinkx = t->x;
			t1->wlinky = t->y;
		    }
		    break;
		}
		if(!findTrack(t->x + 1, t->y) &&
			!findSwitch(t->x + 1, t->y)) {
		    t->elinkx = t1->x;
		    t->elinky = t1->y;
		} else {
		    t->wlinkx = t1->x;
		    t->wlinky = t1->y;
		}
		if(!findTrack(t1->x - 1, t1->y) &&
			!findSwitch(t1->x - 1, t1->y)) {
		    t1->wlinkx = t->x;
		    t1->wlinky = t->y;
		} else {
		    t1->elinkx = t->x;
		    t1->elinky = t->y;
		}
		break;
		
	case SWITCH:
		if(t1->type != SWITCH) {
		    traindir->Error(L("Only like tracks can be linked."));
		    return;
		}
		t->wlinkx = t1->x;
		t->wlinky = t1->y;
		t1->wlinkx = t->x;
		t1->wlinky = t->y;
		break;

	case TSIGNAL:
		if(t1->type != TRACK) {
		    traindir->Error(L("Signals can only be linked to a track."));
		    return;
		}
		t->wlinkx = t1->x;
		t->wlinky = t1->y;
		t->controls = findTrack(t1->x, t1->y);
		break;

	case TRIGGER:
		if(t1->type != TRACK) {
		    traindir->Error(L("Triggers can only be linked to a track."));
		    return;
		}
		t->wlinkx = t1->x;
		t->wlinky = t1->y;
		t->controls = findTrack(t1->x, t1->y);
		break;

	case TEXT:
		if(t1->type != TRACK) {
		    traindir->Error(L("Entry/Exit points can only be linked to a track."));
		    return;
		}
#if 0
		if(t1->direction == TRK_N_S)
		{
		    if(t1->y < t->y) {
			t->elinkx = t1->x;
			t->elinky = t1->y;
		    } else {
			t->wlinkx = t1->x;
			t->wlinky = t1->y;
		    }
		}
#endif
		if(t1->x < t->x) {
		    t->wlinkx = t1->x;
		    t->wlinky = t1->y;
		} else {
		    t->elinkx = t1->x;
		    t->elinky = t1->y;
		}
		break;
	}
}

bool	isInside(Coord& upleft, Coord& downright, int x, int y)
{
	if(x >= upleft.x && x <= downright.x &&
	    y >= upleft.y && y <= downright.y)
	    return true;
	return false;
}

//	Move all track elements in the rectangle
//	comprised by (move_start,move_end) to
//	the coordinarte x,y (upper-left corner)

void	move_layout0(int x, int y)
{
	Coord	start, end;
	int	dx, dy;
	Track	*t, *t1;

	if(move_end.x < move_start.x) {
	    start.x = move_end.x;
	    end.x = move_start.x;
	} else {
	    start.x = move_start.x;
	    end.x = move_end.x;
	}
	if(move_end.y < move_start.y) {
	    start.y = move_end.y;
	    end.y = move_start.y;
	} else {
	    start.y = move_start.y;
	    end.y = move_end.y;
	}
	dx = x - start.x;
	dy = y - start.y;
	for(t = layout; t; t = t->next) {
	    x = t->x;
	    y = t->y;
	    if(isInside(start, end, x, y)) {
		if((t1 = find_track(layout, t->x + dx, t->y + dy)))
		    track_delete(t1);
		t->x += dx;
		t->y += dy;
	    }
	    if(t->elinkx && t->elinky &&
		isInside(start, end, t->elinkx, t->elinky)) {
		t->elinkx += dx;
		t->elinky += dy;
	    }
	    if(t->wlinkx && t->wlinky &&
		isInside(start, end, t->wlinkx, t->wlinky)) {
		t->wlinkx += dx;
		t->wlinky += dy;
	    }
	}

	//  I hope this is right :)

	Itinerary   *it;
	int	    n;

	for(it = itineraries; it; it = it->next) {
	    for(n = 0; n < it->nsects; ++n) {
		if(isInside(start, end, it->sw[n].x, it->sw[n].y)) {
		    it->sw[n].x += dx;
		    it->sw[n].y += dy;
		}
	    }
	}
}

void	move_layout(int x, int y)
{
	// avoid overlaps by moving the original tracks
	// to an area where there cannot be any other track
	move_layout0(move_start.x + 1000, move_start.y + 1000);
	// move back from the temporary area to the
	// final destination area.
	move_start.x += 1000;
	move_start.y += 1000;
	move_end.x += 1000;
	move_end.y += 1000;
	move_layout0(x, y);
	move_start.x -= 1000;
	move_start.y -= 1000;
	move_end.x -= 1000;
	move_end.y -= 1000;
}

void	auto_link_track(Track *t)
{
	int	x, y;
	Track	*t1;

	x = t->x;
	y = t->y;
	switch(t->direction) {
	case W_E:   ++y;    break;
	case E_W:   --y;    break;
	case N_S:   --x;    break;
	case S_N:   ++x;    break;
	}
	t1 = findTrack(x, y);
	if(t1 && t1->type == TRACK &&
		(t1->direction == W_E || t1->direction == TRK_N_S))
	    link_tracks(t, t1);
}

int	macro_select(void)
{
	Track	*t;
	Itinerary *nextItin, *itinList = NULL;	// +Rask Ingemann Lambertsen
	wxChar	buff[256];

	if(!macros) {
	    maxmacros = 1;
	    macros = (Track **)calloc(sizeof(Track *), maxmacros);
	}
	buff[0] = 0;
	if(!traindir->OpenMacroFileDialog(buff))
	    return 0;
	remove_ext(buff);
	if(!(t = load_field_tracks(buff, &itinList)))
	    return 0;
	if(current_macro_name)
	    free(current_macro_name);
	current_macro_name = wxStrdup(buff);
	clean_field(t);
	for(; itinList; itinList = nextItin) {	// +Rask Ingemann Lambertsen
	    nextItin = itinList->next;
	    free_itinerary(itinList);
	}
/*	if(macros[0])
	    clean_field(macros[0]);
	macros[0] = t;
	current_macro = 0;
	nmacros = 1;
	maxmacros = 1;
*/
	return 1;
}

// begin +Rask Ingemann Lambertsen
static void relocate_itinerary(Itinerary *it, int xbase, int ybase)
{
	int	i;

	for(i = 0; i < it->nsects; ++i) {
	    it->sw[i].x += xbase;
	    it->sw[i].y += ybase;
	}
}
// end +Rask Ingemann Lambertsen

void	macro_place(int xbase, int ybase)
{
	Track	*mp;
	Track	*t, *t1;
	int	x, y;
	int	oldtool;
	Itinerary *itn, *itinList = NULL;	// +Rask Ingemann Lambertsen

	if(!current_macro_name)
	    return;
	oldtool = current_tool;
	mp = load_field_tracks(current_macro_name, &itinList);
	while(mp) {
	    t1 = mp->next;
	    x = mp->x + xbase;
	    y = mp->y + ybase;
	    if((t = findTrack(x, y)) || (t = findSwitch(x, y)) ||
		(t = (Track *)findSignal(x, y)) || (t = findText(x, y)) ||
		(t = findPlatform(x, y)) || (t = findImage(x, y)) ||
		(t = findTrackType(x, y, ITIN)) ||
		(t = findTrackType(x, y, TRIGGER))) {
		track_delete(t);
	    }
	    mp->x = x;
	    mp->y = y;
	    if(mp->elinkx || mp->elinky) {
		mp->elinkx += xbase;
		mp->elinky += ybase;
	    }
	    if(mp->wlinkx || mp->wlinky) {
		mp->wlinkx += xbase;
		mp->wlinky += ybase;
	    }
	    mp->next = layout;
	    layout = mp;
	    link_all_tracks();
	    mp = t1;
	    layout_modified = 1;
	}
	// begin +Rask Ingemann Lambertsen
	/* Link in the itineraries from the macro.  Delete duplicates  */
	if(itinList) {
	    for(itn = itinList; itn->next; itn = itn->next) {
		relocate_itinerary(itn, xbase, ybase);
		delete_itinerary(itn->name);
	    }
	    relocate_itinerary(itn, xbase, ybase);
	    delete_itinerary(itn->name);
	    itn->next = itineraries;
	    itineraries = itinList;
	    layout_modified = 1;
	}
	// end+Rask Ingemann Lambertsen
	sort_itineraries();
	invalidate_field();
	repaint_all();
	current_tool = oldtool;
}

void	track_place(int x, int y)
{
	Track	*t, *t1;
	int	needall;

	if(current_tool >= 0 && tooltbl[current_tool].type == MACRO) {
	    if(!current_macro_name || tooltbl[current_tool].direction == 0) {
		select_tool(current_tool - 1);
		return;
	    }
	    macro_place(x, y);
	    return;
	}
	if(current_tool >= 0 && tooltbl[current_tool].type == MOVER) {
	    if(tooltbl[current_tool].direction == 0) {
		move_start.x = x;
		move_start.y = y;
		move_end.x = move_end.y = -1;
		select_tool(current_tool + 1);
		return;
	    }
	    if((short)move_start.x == -1) {
		select_tool(current_tool - 1);
		return;
	    }
	    if((short)move_end.x == -1) {
		move_end.x = x;
		move_end.y = y;
		select_tool(current_tool + 1);
		mover_draw();
		return;
	    }
#if 0
	    // avoid overlaps by moving the original tracks
	    // to an area where there cannot be any other track
	    move_layout(move_start.x + 1000, move_start.y + 1000);
	    // move back from the temporary area to the
	    // final destination area.
	    move_start.x += 1000;
	    move_start.y += 1000;
	    move_end.x += 1000;
	    move_end.y += 1000;
	    move_layout(x, y);
#else
	    move_layout(x, y);
#endif
	    layout_modified = 1;
	    invalidate_field();
	    repaint_all();
	    select_tool(current_tool - 2);
	    move_start.x = move_start.y = -1;
	    move_end.x = move_end.y = -1;
	    return;
	}
	if(current_tool >= 0 && tooltbl[current_tool].type == LINK) {
	    if(tooltbl[current_tool].direction == 0) {
		if(!findTrack(x, y) && !findSignal(x, y) &&
			!findSwitch(x, y) && !findText(x, y) &&
			!findTrackType(x, y, TRIGGER))
		    return;		/* there must be a track */
		link_start.x = x;
		link_start.y = y;
		select_tool(current_tool + 1);
		return;
	    }
	    if(link_start.x == -1) {
		select_tool(current_tool - 1);
		return;
	    }
	    if(!(t = findTrack(link_start.x, link_start.y)) &&
		    !(t = findSwitch(link_start.x, link_start.y)) &&
		    !(t = (Track *)findSignal(link_start.x, link_start.y)) &&
		    !(t = findText(link_start.x, link_start.y)) &&
		    !(t = findTrackType(link_start.x, link_start.y, TRIGGER))) {
		return;
	    }
	    if(!(t1 = findTrack(x, y)) && !(t1 = (Track *)findSignal(x, y)) &&
			!(t1 = findSwitch(x, y)) && !(t1 = findText(x, y))) {
		return;
	    }
	    if(t->type == TRIGGER && t1->type != TRACK)
		return;
	    link_start.x = -1;
	    link_start.y = -1;
	    link_tracks(t, t1);
	    layout_modified = 1;
	    select_tool(current_tool - 1);
	    return;
	}
	needall = 0;
	if((t = findTrack(x, y)) || (t = findSwitch(x, y)) ||
	   (t = (Track *)findSignal(x, y)) || (t = findText(x, y)) ||
	   (t = findPlatform(x, y)) || (t = findImage(x, y)) ||
	   (t = findTrackType(x, y, ITIN)) ||
	   (t = findTrackType(x, y, TRIGGER))) {
	    needall = 1;
	    track_delete(t);
	    link_all_tracks();
	    layout_modified = 1;
	}
	if(current_tool == 0) {		/* delete element */
	    repaint_all();
	    return;
	}
	t = track_new();
	t->x = x;
	t->y = y;
	t->type = (trktype)tooltbl[current_tool].type;
	t->direction = (trkdir)tooltbl[current_tool].direction;
	t->next = layout;
	if(t->type == TEXT)
	    t->station = wxStrdup(wxT("Abc"));
	else if(t->type == IMAGE)
	    t->direction = (trkdir)0;
	else if(t->type == TSIGNAL) {
	    if(t->direction & 2) {
		t->fleeted = 1;
		t->direction = (trkdir)((int)t->direction & (~2));
	    } else
		t->fleeted = 0;
	    if(auto_link)
		auto_link_track(t);
	} else if(t->type == TRIGGER && auto_link)
	    auto_link_track(t);
	layout = t;
	link_all_tracks();
	layout_modified = 1;
	if(needall || is_windows)
	    repaint_all();
	else
	    track_paint(t);
}

void	track_properties(int x, int y)
{
	Track	*t;
	Signal	*sig;
	wxChar	buff[256];

	if((t = findImage(x, y))) {
	    buff[0] = 0;
	    if(t->station)
		wxStrcpy(buff, t->station);
	    if(!traindir->OpenImageDialog(buff))
		return;
	    remove_ext(buff);
	    wxStrcat(buff, wxT(".xpm"));
	    if(t->station)
		free(t->station);
	    t->pixels = 0;
	    t->station = wxStrdup(buff);
	    layout_modified = 1;
	    repaint_all();
	    return;
	}
	if((sig = findSignal(x,y)) && signal_properties_dialog) {
	    signal_properties_dialog(sig);
	    layout_modified = 1;
	    return;
	}
	
	if((t = findTrackType(x, y, TRIGGER)) && trigger_properties_dialog) {
	    trigger_properties_dialog(t);
	    layout_modified = 1;
	    return;
	}

	if((t = findTrack(x, y)) || (t = findText(x, y)) ||
			(t = (Track *)findSignal(x, y)) || /* (t = findImage(x, y)) || */
			(t = findTrackType(x, y, ITIN)) ||
			(t = findTrackType(x, y, TRIGGER))) {
	    track_properties_dialog(t);
	    layout_modified = 1;
	}
}

//
//	Scripting support
//


bool	Track::GetPropertyValue(const wxChar *prop, ExprValue& result)
{
	Track	*t = this;

	// move to Track::GetPropertyValue()
	if(!wxStrcmp(prop, wxT("length"))) {
	    result._op = Number;
	    result._val = t->length;
	    wxSnprintf(expr_buff + wxStrlen(expr_buff), sizeof(expr_buff)/sizeof(wxChar) - wxStrlen(expr_buff), wxT("{%d}"), result._val);
	    return true;
	}
	if(!wxStrcmp(prop, wxT("station"))) {
	    result._op = String;
	    result._txt = t->station ? wxT("") : t->station;
	    wxSnprintf(expr_buff + wxStrlen(expr_buff), sizeof(expr_buff)/sizeof(wxChar) - wxStrlen(expr_buff), wxT("{%s}"), result._txt);
	    return true;
	}
	if(!wxStrcmp(prop, wxT("busy"))) {
	    result._op = Number;
	    result._val = (t->fgcolor != conf.fgcolor) || findTrain(t->x, t->y);
	    wxSnprintf(expr_buff + wxStrlen(expr_buff), sizeof(expr_buff)/sizeof(wxChar) - wxStrlen(expr_buff), wxT("{%d}"), result._val);
	    return true;
	}
	if(!wxStrcmp(prop, wxT("free"))) {
	    result._op = Number;
	    result._val = t->fgcolor == conf.fgcolor && !findTrain(t->x, t->y);
	    wxSnprintf(expr_buff + wxStrlen(expr_buff), sizeof(expr_buff)/sizeof(wxChar) - wxStrlen(expr_buff), wxT("{%d}"), result._val);
	    return true;
	}
	if(!wxStrcmp(prop, wxT("thrown"))) {
	    result._op = Number;
	    if(t->type == SWITCH) {
/*		switch(t->direction) {
		case 10:	// Y switches could be considered always set to a siding
		case 11:	// but it conflicts with the option of reading the status
		case 22:	// then throwing the switch, so this is not enabled.
		case 23:
		    result._val = 1;
		    break;

		default: */
		    result._val = t->switched;
		//}
	    } else
		result._val = 0;
	    wxSnprintf(expr_buff + wxStrlen(expr_buff), sizeof(expr_buff)/sizeof(wxChar) - wxStrlen(expr_buff), wxT("{%d}"), result._val);
	    return true;
	}
	if(!wxStrcmp(prop, wxT("color"))) {
	    result._op = String;
	    result._txt = GetColorName(t->fgcolor);
	    wxSnprintf(expr_buff + wxStrlen(expr_buff), sizeof(expr_buff)/sizeof(wxChar) - wxStrlen(expr_buff), wxT("{%d}"), result._val);
	    return true;
	}

	result._op = Number;
	result._val = 0;
	return false;
}

bool	Track::SetPropertyValue(const wxChar *prop, ExprValue& val)
{
	if(!wxStrcmp(prop, wxT("thrown"))) {
	    wxSnprintf(expr_buff + wxStrlen(expr_buff), sizeof(expr_buff)/sizeof(wxChar) - wxStrlen(expr_buff), wxT("=%d"), val._val);
	    if(type != SWITCH)
		return false;
	    switched = val._val != 0;
	    return true;
	}
	if(!wxStrcmp(prop, wxT("click"))) {
	    wxSnprintf(expr_buff + wxStrlen(expr_buff), sizeof(expr_buff)/sizeof(wxChar) - wxStrlen(expr_buff), wxT("=%d"), val._val);
	    track_selected(this->x, this->y);
	    return true;
	}

	return false;
}


TrackBase::TrackBase()
{
	next = 0;
	next1 = 0;
	x = y = 0;
	xsize = ysize = 0;
	type = NOTRACK;
	direction = W_E;
	status = ST_FREE;
	wlinkx = wlinky = 0;
	elinkx = elinky = 0;
	isstation = 0;
	switched = 0;
	busy = 0;
	fleeted = 0;
	nowfleeted = 0;
	norect = 0;
	fixedred = 0;
	nopenalty = 0;
	noClickPenalty = 0;
	invisible = 0;
	wtrigger = 0;
	etrigger = 0;
	signalx = 0;
	aspect_changed = 0;
	flags = 0;		/* performance flags (TFLG_*) */
	station = 0;
	lock = 0;
	memset(speed, 0, sizeof(speed));
	icon = 0;
	length = 0;
	wsignal = 0;		/* signal controlling this track */
	esignal = 0;		/* signal controlling this track */
	controls = 0;		/* track controlled by this signal */
	fgcolor = 0;
	pixels = 0;		/* for IMAGE pixmap */
	km = 0;			/* station distance (in meters) */
	stateProgram = 0;	/* 4.0: name of function describing state changes */
	_currentState = 0;	/* 4.0: name of current state in state program */
	_interpreterData = 0;	/* 4.0: intermediate data for program interpreter */
	_isFlashing = 0;	/* 4.0: flashing signal */
	_isShuntingSignal = 0;	/* 4.0: only affects shunting trains */
	_nextFlashingIcon = 0;	/* 4.0: index in list of icons when flashing */
	_fontIndex = 0;
}

bool	TrackInterpreterData::Evaluate(ExprNode *n, ExprValue& result)
{
	Track	*t = 0;
	Signal	*sig = 0;
	ExprValue left(None);
	ExprValue right(None);
	const wxChar	*prop;

	if(!n)
	    return false;
        switch(n->_op) {

	case Dot:
	    
	    result._op = Addr;
	    if(!(n->_left)) {
		result._track = this->_track;
		result._op = TrackRef;
	    } else {
		if(!Evaluate(n->_left, result))
		    return false;

		if(result._op == TrainRef)
		    goto not_track;
		if(result._op == SignalRef)
		    goto not_track;
		if(result._op != TrackRef)
		    return false;
	    }
	    if(!result._track) {
		wxStrcat(expr_buff, wxT("no current track for '.'"));
		return false;
	    }
	    t = result._track;
	    TraceCoord(t->x, t->y);

not_track:
	    result._txt = n->_txt;
	    if(_forAddr)
		return true;

	    prop = n->_txt;
	    if(!prop)
		return false;

	    switch(result._op) {
	    
	    case SwitchRef:

		if(!wxStrcmp(prop, wxT("thrown"))) {
		    result._op = Number;
		    result._val = t->switched;
		    return true;
		}

	    case Addr:
	    case TrackRef:

		if(!result._track)
		    return false;
		return result._track->GetPropertyValue(prop, result);

	    case SignalRef:

		if(!result._signal)
		    return false;
		return result._signal->GetPropertyValue(prop, result);

	    case TrainRef:

		if(!result._train)
		    return false;
		return result._train->GetPropertyValue(prop, result);

	    }
	    return false;

	case Equal:
	    
	    result._op = Number;
	    result._val = 0;
	    //if(_forCond)
		return InterpreterData::Evaluate(n, result);
	    //return false;

	default:

	    return InterpreterData::Evaluate(n, result);
	}

	return false;
}


void	Track::OnInit()
{
	TrackInterpreterData *interp = (TrackInterpreterData *)_interpreterData;
	if(!interp)
	    return;
	if(!interp->_onInit)
	    return;
	interp->_track = this;
	interp->_train  = 0;
	interp->_signal = 0;
	interp->_stackPtr = 0;
	interp->TraceCoord(x, y, wxT("Track::OnInit"));
	Trace(expr_buff);
	interp->Execute(interp->_onInit);
	return;
}

void	Track::OnSetBusy()
{
	TrackInterpreterData *interp = (TrackInterpreterData *)_interpreterData;
	if(!interp)
	    return;
	if(!interp->_onSetBusy)
	    return;
	interp->_track = this;
	interp->_train  = 0;
	interp->_signal = 0;
	interp->_stackPtr = 0;
	interp->TraceCoord(x, y, wxT("Track::OnSetBusy"));
	Trace(expr_buff);
	interp->Execute(interp->_onSetBusy);
	return;
}

void	Track::OnSetFree()
{
	TrackInterpreterData *interp = (TrackInterpreterData *)_interpreterData;
	if(!interp)
	    return;
	if(!interp->_onSetFree)
	    return;
	interp->_track = this;
	interp->_train  = 0;
	interp->_signal = 0;
	interp->_stackPtr = 0;
	interp->TraceCoord(x, y, wxT("Track::OnSetFree"));
	Trace(expr_buff);
	interp->Execute(interp->_onSetFree);
	return;
}

void	Track::OnEnter(Train *trn)
{
	TrackInterpreterData *interp = (TrackInterpreterData *)_interpreterData;
	if(!interp)
	    return;
	if(!interp->_onEnter)
	    return;
	interp->_track = this;
	interp->_train = trn;
	interp->_stackPtr = 0;
	interp->_signal = 0;
	interp->TraceCoord(x, y, wxT("Track::OnEnter"));
	Trace(expr_buff);
	interp->Execute(interp->_onEnter);
	return;
}

void	Track::OnExit(Train *trn)
{
	TrackInterpreterData *interp = (TrackInterpreterData *)_interpreterData;
	if(!interp)
	    return;
	if(!interp->_onExit)
	    return;
	interp->_track = this;
	interp->_train = trn;
	interp->_stackPtr = 0;
	interp->_signal = 0;
	interp->TraceCoord(x, y, wxT("Track::OnExit"));
	Trace(expr_buff);
	interp->Execute(interp->_onExit);
	return;
}

void	Track::OnClicked()
{
	TrackInterpreterData *interp = (TrackInterpreterData *)_interpreterData;
	if(!interp)
	    return;
	if(!interp->_onClicked)
	    return;
	interp->_track = this;
	interp->_train = 0;
	interp->_signal = 0;
	interp->_stackPtr = 0;
	interp->TraceCoord(x, y, wxT("Track::OnClicked"));
	Trace(expr_buff);
	interp->Execute(interp->_onClicked);
	return;
}

void	Track::OnCrossed(Train *trn)
{
	TrackInterpreterData *interp = (TrackInterpreterData *)_interpreterData;
	if(!interp)
	    return;
	if(!interp->_onCrossed)
	    return;
	interp->_track = this;
	interp->_train = trn;
	interp->_signal = 0;
	interp->_stackPtr = 0;
	interp->TraceCoord(x, y, wxT("Track::OnCrossed"));
	Trace(expr_buff);
	interp->Execute(interp->_onCrossed);
	return;
}

void	Track::ParseProgram()
{
	const wxChar	*p;

	if(!this->stateProgram || !*this->stateProgram)
	    return;
	if(_interpreterData)	    // previous script
	    delete _interpreterData;
	_interpreterData = new TrackInterpreterData;

	TrackInterpreterData *interp = (TrackInterpreterData *)_interpreterData;
	p = this->stateProgram;
	while(*p) {
	    const wxChar	*p1 = p;
	    while(*p1 == ' ' || *p1 == '\t' || *p1 == '\r' || *p1 == '\n')
		++p1;
	    p = p1;
	    if(match(&p, wxT("OnInit:"))) {
		p1 = p;
		interp->_onInit = ParseStatements(&p);
	    } else if(match(&p, wxT("OnSetBusy:"))) {
		p = next_token(p);
		p1 = p;
		interp->_onSetBusy = ParseStatements(&p);
	    } else if(match(&p, wxT("OnSetFree:"))) {
		p = next_token(p);
		p1 = p;
		interp->_onSetFree = ParseStatements(&p);
	    } else if(match(&p, wxT("OnEnter:"))) {
		p = next_token(p);
		p1 = p;
		interp->_onEnter = ParseStatements(&p);
	    } else if(match(&p, wxT("OnCrossed:"))) {
		p = next_token(p);
		p1 = p;
		interp->_onCrossed = ParseStatements(&p);
	    } else if(match(&p, wxT("OnExit:"))) {
		p = next_token(p);
		p1 = p;
		interp->_onExit = ParseStatements(&p);
	    } else if(match(&p, wxT("OnClicked:"))) {
		p = next_token(p);
		p1 = p;
		interp->_onClicked = ParseStatements(&p);
	    }
	    if(p1 == p)	    // error! couldn't parse token
		break;
	}
}

void	Track::RunScript(const Char *script, Train *trn)
{
	Script	*s = find_script(script);
	if(!s) {
	    s = new_script(script);
	    // return;
	}
	if(!s->ReadFile())
	    return;

	// is it different from current one?
	if(!this->stateProgram || wxStrcmp(s->_text, this->stateProgram)) {
	    if(this->stateProgram)
		free(this->stateProgram);
	    this->stateProgram = wxStrdup(s->_text);
	    ParseProgram();
	}
	OnEnter(trn);
}


void	Track::SetColor(grcolor color)
{
	if(this->fgcolor == color)
	    return;
	this->fgcolor = color;
	change_coord(this->x, this->y);
	if(color == conf.fgcolor)
	    OnSetFree();
	else
	    OnSetBusy();
}

bool	Track::IsBusy() const
{
	if(this->fgcolor != conf.fgcolor)
	    return true;
	return false;
}

