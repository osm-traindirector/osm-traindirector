$(OBJDIR)Canvas.o: Canvas.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) Canvas.cpp

$(OBJDIR)html.o: html.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) html.cpp

$(OBJDIR)http.o: http.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) http.cpp

$(OBJDIR)AlertList.o: AlertList.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) AlertList.cpp

$(OBJDIR)TrainInfoList.o: TrainInfoList.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) TrainInfoList.cpp

$(OBJDIR)AssignDialog.o: AssignDialog.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) AssignDialog.cpp

$(OBJDIR)DaysDialog.o: DaysDialog.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) DaysDialog.cpp

$(OBJDIR)ItineraryDialog.o: ItineraryDialog.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) ItineraryDialog.cpp

$(OBJDIR)ConfigDialog.o: ConfigDialog.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) ConfigDialog.cpp

$(OBJDIR)OptionsDialog.o: OptionsDialog.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) OptionsDialog.cpp

$(OBJDIR)SignalDialog.o: SignalDialog.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) SignalDialog.cpp

$(OBJDIR)TrackDialog.o: TrackDialog.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) TrackDialog.cpp

$(OBJDIR)TriggerDialog.o: TriggerDialog.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) TriggerDialog.cpp

$(OBJDIR)StationInfoDialog.o: StationInfoDialog.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) StationInfoDialog.cpp

$(OBJDIR)TrainInfoDialog.o: TrainInfoDialog.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) TrainInfoDialog.cpp

$(OBJDIR)loadsave.o: loadsave.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) loadsave.cpp

$(OBJDIR)Main.o: Main.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) Main.cpp

$(OBJDIR)MainFrm.o: MainFrm.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) MainFrm.cpp

$(OBJDIR)run.o: run.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) run.cpp

$(OBJDIR)TConfig.o: TConfig.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) TConfig.cpp

$(OBJDIR)TDFile.o: TDFile.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) TDFile.cpp

$(OBJDIR)ReportBase.o: ReportBase.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) ReportBase.cpp

$(OBJDIR)GraphView.o: GraphView.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) GraphView.cpp

$(OBJDIR)LateGraphView.o: LateGraphView.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) LateGraphView.cpp

$(OBJDIR)HtmlView.o: HtmlView.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) HtmlView.cpp

$(OBJDIR)ItineraryView.o: ItineraryView.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) ItineraryView.cpp

$(OBJDIR)ToolsView.o: ToolsView.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) ToolsView.cpp

$(OBJDIR)TimeTblView.o: TimeTblView.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) TimeTblView.cpp

$(OBJDIR)NotebookMgr.o: NotebookMgr.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) NotebookMgr.cpp

$(OBJDIR)track.o: track.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) track.cpp

$(OBJDIR)Train.o: Train.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) Train.cpp

$(OBJDIR)trsim.o: trsim.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) trsim.cpp

$(OBJDIR)Itinerary.o: Itinerary.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) Itinerary.cpp

$(OBJDIR)Localize.o: Localize.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) Localize.cpp

$(OBJDIR)TSignal.o: TSignal.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) TSignal.cpp

$(OBJDIR)tdscript.o: tdscript.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) tdscript.cpp

$(OBJDIR)ScenarioInfoDialog.o: ScenarioInfoDialog.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) ScenarioInfoDialog.cpp

$(OBJDIR)TrackScriptDialog.o: TrackScriptDialog.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) TrackScriptDialog.cpp

$(OBJDIR)FontManager.o: FontManager.cpp
	$(CXXC) -c -o $@ $(TD3_CXXFLAGS) FontManager.cpp



