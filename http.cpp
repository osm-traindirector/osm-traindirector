#include <wx/sstream.h>
#include <wx/protocol/http.h>
#include "Traindir3.h"

extern Traindir	*traindir;

static
const Char	*locate(const wxChar *p, const wxChar *pattern)
{
	int l = wxStrlen(pattern);

	while(*p && wxStrncmp(p, pattern, l))
	    ++p;
	if(*p)
	    return p;
	return 0;
}

int	get_delay(Train *t)
{
	int	delay = 0;
#ifdef WIN32
	wxHTTP	get;

	if(!use_real_time)
	    return 0;

	get.SetHeader(_T("Content-type"), _T("text/html; charset=utf-8"));
	get.SetTimeout(10); // 10 seconds of timeout instead of 10 minutes ...
 
	// this will wait until the user connects to the internet. It is important in case of dialup (or ADSL) connections
	while (!get.Connect(_T("mobile.viaggiatreno.it")))  // only the server, no pages here yet ...
	    wxSleep(5);
 
	traindir->IsMainLoopRunning(); // should return true
 
	wxChar buff[256];
	int i, j = 0;

	for(i = 0; t->name[i]; ++i) {
	    if(!wxIsdigit(t->name[i]))
		continue;
	    buff[j++] = t->name[i];
	}
	buff[j] = 0;

	wxChar	url[256];

	wxSnprintf(url, sizeof(url), wxT("/viaggiatreno/mobile/scheda?numeroTreno=%s&tipoRicerca=numero"), buff);
	// use _T("/") for index.html, index.php, default.asp, etc.
	wxInputStream *httpStream = get.GetInputStream(url);
 
	// wxLogVerbose( wxString(_T(" GetInputStream: ")) << get.GetResponse() << _T("-") << ((resStream)? _T("OK ") : _T("FAILURE ")) << get.GetError() );
 
	if (get.GetError() == wxPROTO_NOERR)
	{
	    wxString res;
	    wxStringOutputStream out_stream(&res);
	    httpStream->Read(out_stream);
	    //wxMessageBox(res);
 
	    const Char *p = res.c_str();
	    const Char *line = p;

	    if((p = locate(p, wxT("<!-- SITUAZIONE")))) {
		while(*p && wxStrncmp(p, wxT("minuti di ritardo"), 17)) {
		    ++p;
		    if(*p == '\n')
			line = p;
		}
		if(*p) {
		    for(p = line; *p && !wxIsdigit(*p); ++p);
		    if(wxIsdigit(*p))
			delay = wxAtoi(p);
		}
	    }

	    // wxLogVerbose( wxString(_T(" returned document length: ")) << res.Length() );
	}
	else
	{
	    wxMessageBox(_T("Unable to connect!"));
	}
 
	wxDELETE(httpStream);
	get.Close();
#endif
	return delay;
}
