/*	OptionsDialog.cpp - Created by Giampiero Caprino

This file is part of Train Director 3

Train Director is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; using exclusively version 2.
It is expressly forbidden the use of higher versions of the GNU
General Public License.

Train Director is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Train Director; see the file COPYING.  If not, write to
the Free Software Foundation, 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.
*/

#include <wx/wxprec.h>
#include <wx/sizer.h>
#include <wx/button.h>
#include <wx/radiobox.h>
#include <wx/statline.h>

#include "Traindir3.h"
#include "OptionsDialog.h"

static	struct opt {
    const wxChar	*name;
    int		*optp;
} opts[NUM_OPTIONS + 1] = {
    { wxT("Short train info"), &terse_status },
    { wxT("Status line at top of window"), &status_on_top },
    { wxT("Alert sound on"), &beep_on_alert },
    { wxT("Alert on train entering layout"), &beep_on_enter },
    { wxT("View speed limits"), &show_speeds },
    { wxT("Automatically link signals"), &auto_link },
    { wxT("Show grid"), &show_grid },
    { wxT("View long blocks"), &show_blocks },
    { wxT("Show seconds on clock"), &show_seconds },
    { wxT("Traditional signals"), &signal_traditional },
    { wxT("Strong performance checking"), &hard_counters },
    { wxT("Show linked objects in editor"), &show_links },
    { wxT("Show trains icons"), &show_icons },
    { wxT("Show trains tooltip"), &show_tooltip },
//    { wxT("Check real-time train status"), &use_real_time },
    0
};

OptionsDialog::OptionsDialog(wxWindow *parent)
: wxDialog(parent, 0, L("Preferences"), wxDefaultPosition, wxDefaultSize,
	   wxDEFAULT_DIALOG_STYLE, L("Preferences"))
{
	int		i;
	wxArrayString   strings;
	wxBoxSizer	*column = new wxBoxSizer( wxVERTICAL );
	wxBoxSizer	*column2 = new wxBoxSizer( wxVERTICAL );
	wxBoxSizer	*column3 = new wxBoxSizer( wxVERTICAL );
	wxBoxSizer	*row = new wxBoxSizer( wxHORIZONTAL );
    
	wxStaticText    *header = new wxStaticText( this, 0, 
	    L("Check the desired options:"));
    
	column3->Add(header, 0, wxALL, 10);

	for(i = 0; opts[i].name; ++i) {

	    m_boxes[i] = new wxCheckBox( this, ID_CHECKBOX,
		LV(opts[i].name), wxDefaultPosition, wxDefaultSize);

	    if(i > 6) {
		column2->Add(m_boxes[i], 0, wxLEFT, 10);
		column2->AddSpacer(6);
	    } else {
		column->Add(m_boxes[i], 0, wxLEFT, 10);
		column->AddSpacer(6);
	    }
	}

	row->Add(column, 1, wxGROW|wxRIGHT, 5);
	row->Add(column2, 1, wxGROW|wxRIGHT, 5);

	column3->Add(row, 0, wxALL, 10);

	wxStaticLine *line = new wxStaticLine( this );
	column3->Add(line, 1, wxGROW|wxTOP|wxBOTTOM, 5);
	
	column3->Add(CreateButtonSizer(wxOK | wxCANCEL), 0, wxGROW | wxALL, 10);

	SetSizer(column3);
	column3->Fit(this);
	column3->SetSizeHints(this);
}

OptionsDialog::~OptionsDialog()
{
}

int	OptionsDialog::ShowModal()
{
	int	    i;
	int	    res;

	for(i = 0; opts[i].name; ++i) {
	    m_boxes[i]->SetValue(*opts[i].optp != 0);
	}
	Centre();
	bool oldIgnore = traindir->m_ignoreTimer;
	traindir->m_ignoreTimer = true;
	res = wxDialog::ShowModal();
	traindir->m_ignoreTimer = oldIgnore;
	if(res == wxID_OK) {
	    for(i = 0; opts[i].name; ++i) {
		*opts[i].optp = m_boxes[i]->GetValue() ? 1 : 0;
	    }
	}
	return res;
}
